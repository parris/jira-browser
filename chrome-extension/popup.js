chrome.storage.sync.get(null, (options) => {
    // First get the JIRA projects
    const projectsTable = document.getElementById('projects-table');

    for (let project of options.projects) {
        const row = projectsTable.insertRow();

        const label = row.insertCell(0);
        label.innerHTML =`<label for="${project}-ticket-number-input">${project}-</label>`;

        const input = row.insertCell(1);
        input.innerHTML = `<input id="${project}-ticket-number-input" class="ticket-number-input" data-project="${project}" type="text"/>`;
    }

    // Then get the Salesforce sandboxes
    const sandboxesTable = document.getElementById('sandboxes-table');

    for (let sandbox of options.sandboxes) {
        const row = sandboxesTable.insertRow();

        const label = row.insertCell(0);
        label.innerHTML =`<label for="${sandbox}-sf-objectid-input">${sandbox}: </label>`;

        const input = row.insertCell(1);
        input.innerHTML = `<input id="${sandbox}-sf-objectid-input" class="sf-objectid-input" data-sandbox="${sandbox}" type="text"/>`;
    }

    // Then add the ENTER listeners
    projectsTable.addEventListener('keypress', function (e) {
        if (!e.target) {
            return;
        }

        if (e.key === 'Enter') {
            const project = e.target.dataset.project;
            const ticketNum = e.target.value;
            openJiraTab(project, ticketNum);
        }
    });

    // And a similar event listener for PASTE
    sandboxesTable.addEventListener('keypress', function (e) {
        if (!e.target ||  !e.target.matches('.sf-objectid-input')) {
            return;
        }

        if (e.key === 'Enter') {
            const sandbox  = e.target.dataset.sandbox;
            const objectId = e.target.value;
            openSalesforceTab(sandbox, objectId);
        }
    });

    // And a similar event listener for PASTE
    projectsTable.addEventListener('paste', function (e) {
        if (!e.target ||  !e.target.matches('.ticket-number-input')) {
            return;
        }

        const project   = e.target.dataset.project;
        const ticketNum = e.clipboardData.getData('text');
        openJiraTab(project, ticketNum);
    });

    // And a similar event listener for PASTE
    sandboxesTable.addEventListener('paste', function (e) {
        if (!e.target ||  !e.target.matches('.sf-objectid-input')) {
            return;
        }

        const sandbox  = e.target.dataset.sandbox;
        const objectId = e.clipboardData.getData('text');
        openSalesforceTab(sandbox, objectId);
    });

    // Show the JIRA base url option
    const jiraBaseUrls = document.getElementsByClassName('jiraBaseUrl');

    for (let j of jiraBaseUrls) {
        j.innerHTML = options.jiraBaseUrl;
    }

    // Show the Salesforce base url option
    const salesforceBaseUrls = document.getElementsByClassName('salesforceBaseUrl');

    for (let s of salesforceBaseUrls) {
        s.innerHTML = options.salesforceBaseUrl;
    }
});

function openJiraTab(project, ticketNum) {
    chrome.storage.sync.get(null, (options) => {
        // Strip of the PROJECT- part of PROJECT-1234 because it is added back on next step
        ticketNum = ticketNum.replace(`${project}-`, '');

        // Open ticket in new tab
        window.open(`${options.jiraBaseUrl}/browse/${project}-${ticketNum}`);
    });
}

function openSalesforceTab(sandbox, objectId) {
    chrome.storage.sync.get(null, (options) => {
        // Sandboxes are lowercase
        sandbox = sandbox.toLowerCase();

        // Get base url subdomain
        let subdomain = options.salesforceBaseUrl.match(/^https:\/\/([^.]+)/)[1];
        let sandboxSubdomain = subdomain;

        // Append sandbox to subdomain (but don't append anything if Prod sandbox)
        if (sandbox !== 'prod') {
            sandboxSubdomain += `--${sandbox}`;
        }


        const url = options.salesforceBaseUrl.replace(subdomain, sandboxSubdomain);

        // Open ticket in new tab
        window.open(`${url}/lightning/r/Opportunity/${objectId}/view`);
    });
}