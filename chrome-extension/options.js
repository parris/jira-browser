document.querySelector('#options-save-button').addEventListener('click', saveOptions);

// Saves options to chrome.storage
function saveOptions() {
    return Promise.all([
        // Save jira options
        saveJiraOptions(),

        // Save Salesforce options
        saveSalesforceOptions()
    ]).then(r => alert("Saved"));
}

function saveJiraOptions() {
    // Get the jira base url
    let jiraBaseUrl = document.getElementById("jira-url").value;

    // Trim trailing slash, if it exists
    jiraBaseUrl = jiraBaseUrl.replace(/\/$/, '');

    // Trim "/browse", if it exists
    jiraBaseUrl = jiraBaseUrl.replace(/\/browse$/, '');

    // Trim leading protocol, if it exists, don't support if no ssl
    jiraBaseUrl = jiraBaseUrl.replace(/^https:\/\//, '');

    // Put the protocol back
    jiraBaseUrl = `https://${jiraBaseUrl}`;

    // Get list of projects
    const projects = [];
    const projectList = document.getElementById("projects").value;

    // Trim off any white space from projects, and make uppercase
    for (let project of projectList.split(',')) {
        projects.push(project.trim().toUpperCase());
    }

    return new Promise(resolve => {
        // Save options to extension storage
        chrome.storage.sync.set({jiraBaseUrl, projects}, function () {
            resolve();
        });
    });
}

function saveSalesforceOptions() {
    // Get the jira base url
    let salesforceBaseUrl = document.getElementById("salesforce-url").value;

    // Trim trailing slash, if it exists
    salesforceBaseUrl = salesforceBaseUrl.replace(/\/$/, '');

    // Trim leading protocol, if it exists, don't support if no ssl
    salesforceBaseUrl = salesforceBaseUrl.replace(/^https:\/\//, '');

    // Trim any path
    salesforceBaseUrl = salesforceBaseUrl.replace(/\/.+$/, '');

    // Strip out any sandbox/
    const subdomain = salesforceBaseUrl.match(/^[^.]+/)[0];
    const strippedSubdomain = subdomain.replace(/--.+$/, '');
    salesforceBaseUrl = salesforceBaseUrl.replace(subdomain, strippedSubdomain);

    // Put the protocol back
    salesforceBaseUrl = `https://${salesforceBaseUrl}`;

    // Get list of projects
    const sandboxes = [];
    const sandboxList = document.getElementById("sandboxes").value;

    // Trim off any white space from sandboxes, and make uppercase
    for (let sandbox of sandboxList.split(',')) {
        sandboxes.push(sandbox.trim().toUpperCase());
    }

    return new Promise(resolve => {
        // Save options to extension storage
        chrome.storage.sync.set({salesforceBaseUrl, sandboxes}, function () {
            resolve();
        });
    });
}

window.addEventListener('load', loadExistingConfig);

function loadExistingConfig() {
    chrome.storage.sync.get(null, (options) => {
        document.getElementById("jira-url").value = options.jiraBaseUrl || null;
        document.getElementById("projects").value = options.projects || null;
        document.getElementById("salesforce-url").value = options.salesforceBaseUrl || null;
        document.getElementById("sandboxes").value = options.sandboxes || null;
    });
}